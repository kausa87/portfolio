import React from 'react';
import Navbar from './components/Navbar';
import TodoList from './components/TodoList';
import Footer from './components/Footer';
import Authenticate from './components/Authenticate';
// import { Redirect } from 'react-router-dom';
// import MainList from './components/MainList'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faHome } from '@fortawesome/free-solid-svg-icons'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import db from './db.js';

class App extends React.Component {
  state = {
    todos: [],
    completed: [],
    user: [],
    isAuthenticated: '',//'c25735ec-624d-480b-a32f-34b443a61142',
    errorLogin: false,
  };

  componentDidMount() {
    const initialState = db.getState();
    this.setState(initialState);
    console.log('Mi estado inicial', this.state);
  }

  handleAddTodo = (todo) => {
    const newTodos = db.get('todos').push(todo).write();
    this.setState({
      todos: newTodos,
    });
    // this.setState((prevState) => ({
    //   todos: [...prevState.todos, todo],
    // }));
    // db.get('todos').push(todo).write()
    // console.log("Mi estado inicial")
  };

  returnisisCardinTodos = (id) => {
    const todo = this.state.todos.find((elem) => elem.id === id);
    if (todo) return true;
    else return false;
  };

  handleFav = (id) => {
    const isinTodo = this.returnisisCardinTodos(id);
    if (isinTodo) {
      const z = this.state.todos;

      z.filter((elem) => {
        if (elem.id === id) elem.fav = !elem.fav;
        return elem;
      });
      this.setState({
        todo: z,
      });
    } else {
      const z = this.state.completed;

      z.filter((elem) => {
        if (elem.id === id) elem.fav = !elem.fav;
        return elem;
      });
      this.setState({
        completed: z,
      });
    }
  };

  handleAddCompleted = (todo) => {
    const completedList = db.get('completed').push(todo).write();
    this.setState({
      completed: completedList,
    });

  };

  handleLogout=()=>{
    this.setState({isAuthenticated: ''})
  }

  handleRemoveTodo = (id) => {
    const todoList = this.state.todos.filter((todo) => todo.id !== id);
    db.set('todos', todoList).write();
    this.setState({
      todos: todoList,
    });

  };

  handleCompleteTodo = (id) => {
    const todo = this.state.todos.find((elem) => elem.id === id);

    this.handleRemoveTodo(id);
    this.handleAddCompleted(todo);
  };

  handleDeleting = (id) => {
    this.handleRemoveTodo(id);
  };

  handleAuth = async (user, email, password) => {

    if (user) {
      const userList = db.get('user').push(user).write();
      await this.setState({
        user: userList,
      });
      await this.setState({ isAuthenticated: user.id });
    } else {
      //LOGIN
      const isUser = this.state.user.find(
        (elem) => elem.email === email && elem.password === password
      );
      if (isUser) {
        await this.setState({ isAuthenticated: isUser.id });
        await this.setState({ errorLogin: false });
      } else {
        await this.setState({ errorLogin: true });

      }
    }
  };

  handleEditingCard = (userModified) =>{
    const tasks = this.state.todos.filter((elem)=> elem.id !== userModified.cardId)
    console.log("todos sin modified",tasks )
    var newTask =  this.state.todos.find((elem)=> elem.id === userModified.cardId)
    console.log(" modified",newTask )
    newTask.title = userModified.title
    newTask.description = userModified.description
    db.set('todos', [...tasks, newTask]).write();
    this.setState({ todos: [...tasks, newTask] });


  }

  render() {
    const arrayFav = [...this.state.todos, ...this.state.completed].filter(
      (elem) => elem.fav && elem.userId === this.state.isAuthenticated
    );
  
    const filterTodo = this.state.todos.filter(elem => elem.userId === this.state.isAuthenticated)
    const filterCompl = this.state.completed.filter(elem => elem.userId === this.state.isAuthenticated)


    return (
      <Router>
        <div className="App">
          <Navbar isAuthenticated={this.state.isAuthenticated}/>
          <Switch>
            <Route
              path="/" exact
              render={() => (
                <Authenticate
                  handleAuth={this.handleAuth}
                  isAuthenticated={this.state.isAuthenticated}
                  errorLogin={this.state.errorLogin}
                  handleLogout={this.handleLogout}
                />
              )}
            />

      { this.state.isAuthenticated ?
      <React.Fragment>
            <Route
              path="/favorite" exact
              render={() => (
                <TodoList
                  handleFav={this.handleFav}
                  todos={arrayFav}
                  //noCancel="2"
                  title="Favoritos ❤️"
                />
              )}
            />

            <Route
              path="/completed" exact
              render={() => (
                <TodoList
                  handleFav={this.handleFav}
                  todos={filterCompl}
                  noCancel="0"
                  title="Tareas completadas 🚀"
                />
              )}
            />

            <Route
              path="/pendingtasks" exact
              render={() => (
                <TodoList
                  isAuthenticated={this.state.isAuthenticated}
                  title="Tareas pendientes ⏳"
                  noCancel="1"
                  todos={filterTodo}
                  onCompleteCard={this.handleCompleteTodo}
                  onAddCard={this.handleAddTodo}
                  handleFav={this.handleFav}
                  handleDeleting={this.handleDeleting}
                  handleEditingCard={this.handleEditingCard}
                />
              
              )}
            />
          </React.Fragment>
            : null }
          </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
