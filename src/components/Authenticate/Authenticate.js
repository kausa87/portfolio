import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { uuid } from 'uuidv4';
import './style.scss'

class Authenticate extends Component {

    state = {
        email: '',
        password: '',
        isSubmit: false,
        isDisabled: true,
        isEmailValid: false,
        isPasswordValid: false,
        gotAccount: true
    }

    handleOnSubmit = (e)=>{
        e.preventDefault()
        const { value, name } = e.target
        const {isPasswordValid, gotAccount } = this.state

        this.setState({[name]:value,})
        this.setState({isSubmit: true})

        if(isPasswordValid)
            if(gotAccount) this.props.handleAuth(null, this.state.email, this.state.password)
                else this.props.handleAuth( { id: uuid(), email: this.state.email, password: this.state.password}, null, null)



    }

    handleNoAccount = ()=>{
        this.setState({gotAccount: !this.state.gotAccount})
    }

    handleChangeInput= async (e)=>{
        e.preventDefault()
        const { value, name } = e.target
        await this.setState({[name]:value,})
        const psw = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(this.state.password)
        await this.setState({isPasswordValid: psw})
        console.log("mypsw", this.state.password)
        console.log("mypsw is valid ", this.state.isPasswordValid)

    }

    render() {
        const {isSubmit, isPasswordValid} = this.state
        

        return (
            <div className="Authenticate">
        
        {!this.props.isAuthenticated ?
        <form onSubmit={this.handleOnSubmit} autocomplete="off">
        
        {this.state.gotAccount ? <h2>LOGIN</h2> : <h2>REGISTER</h2> }
        {this.props.errorLogin && this.state.gotAccount ? <p className="Authenticate__error">Error login</p> : null }
        <label className="Authenticate__label">
          <input
          className="Authenticate__input"
            type="email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleChangeInput}
          />
        </label>

        <label className="Authenticate__label">
        {isSubmit && !isPasswordValid ? <p className="Authenticate__error">Error en la password</p>: null}
          <input
          className="Authenticate__input"
            type="password"
            name="password"
            placeholder="Contraseña"
            value={this.state.password}
            onChange={this.handleChangeInput}
          />
        </label>
        {!this.state.gotAccount ?
        <p><input type="checkbox" name="checkbox" value="check" id="agree" required/>Estoy de acuerdo con la politica de privacidad</p> : null}
    
        <button
          className="Authenticate__button-add"
          disabled={!isPasswordValid || !this.state.password || !this.state.email }
          type="submit"
        >
          {this.state.gotAccount ? "Iniciar sesión" : "Registrate" }
        </button>
        {this.state.gotAccount ? <p className="Authenticate__linktext" onClick={this.handleNoAccount}> ¿No tienes cuenta? Pincha aquí </p> : <p className="Authenticate__linktext" onClick={this.handleNoAccount}> ¿Ya tienes cuenta? Logueate </p> }
        
      </form>
      :
        <div>
        <h2>Bienvenido</h2>
        <p onClick={this.props.handleLogout}>Si quieres hacer Logout, picha aquí</p></div>
        }
  
            </div>
        )
    }
}


export default Authenticate
