import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import EditCard from '../EditCard';
import {
  faCircle as faCircleSolid,
  faCheckSquare,
  faTimes,
  faCheck,
  faEdit as faEditSolid,
  faTrash as faTrashSolid,
  faHeart as FaHeartSolid,
} from '@fortawesome/free-solid-svg-icons';
import {
  faCircle,
  faDotCircle,
  faEdit as faEditRegular,
  faHeart as FaHeartRegular,
} from '@fortawesome/free-regular-svg-icons';

import './style.scss';

class Card extends Component {
  state = {
    isHovering: false,
    isSolid: false,
    isEditing: false,
  };

  handleOnClick = () => {
    console.log('click');
    const { onCompleteCard, cardId } = this.props;
    onCompleteCard(cardId);
  };

  handleOnMouseEnter = (e) => {
    this.setState({
      isHovering: true,
    });
  };

  handleOnMouseLeave = (e) => {
    this.setState({
      isHovering: false,
      isSolid: false,
    });
  };

  handleOnMouseDown = (e) => {
    this.setState({
      isSolid: true,
    });
  };

  handleOnMouseUp = (e) => {
    this.setState({
      isSolid: false,
    });
  };

  myCallback = () => {
    this.props.handleFav(this.props.cardId);
  };

  myTrashCallback = () => {
    this.props.handleDeleting(this.props.cardId);
  };

  changeModeEdit = () => {
    this.setState({ isEditing: !this.state.isEditing });
  };


  handleEditingCardFilter = (u) =>{
    this.setState({
      isEditing: !this.state.isEditing,
    })
    this.props.handleEditingCard(u)
  }

  

  render() {
    const { isHovering, isSolid } = this.state;
    const circleIcon = isSolid ? faCircleSolid : faCircle;
    const { cardId } = this.props;

    return (
      <li className="Card">
        {this.props.noCancel === "1" ? (
          <span className="Card__trash" onClick={this.myTrashCallback}>
            <FontAwesomeIcon icon={faTrashSolid} />
          </span>
        ) : null}

        <p onClick={this.myCallback}>
          {this.props.fav ? (
            <span className="Card__heart">
              <FontAwesomeIcon icon={FaHeartSolid} />
            </span>
          ) : (
            <span className="Card__heart">
              <FontAwesomeIcon icon={FaHeartRegular} />
            </span>
          )}
        </p>

        {this.props.noCancel ? (
          <span className="Card__close" onClick={this.handleOnClick}>
            {' '}
            <FontAwesomeIcon icon={faCheck} />
          </span>
        ) : null}

        <p className="Card__text">
          {this.props.noCancel ? null : (
            <span className="Card__check">
              <FontAwesomeIcon icon={faCheck} />
            </span>
          )}
        </p>


        {/* {this.state.isEditing ? (
          <span onClick={this.changeModeEdit} className="Card__edit">
            <FontAwesomeIcon icon={faEditSolid} />
          </span>
        ) : (
          <span onClick={this.changeModeEdit} className="Card__edit">
            <FontAwesomeIcon icon={faEditRegular} />
          </span>
        )} */}

        {
          this.state.isEditing && this.props.noCancel === "1" ?
          <span onClick={this.changeModeEdit} className="Card__edit">
            <FontAwesomeIcon icon={faEditSolid} />
          </span> : null
        }


{
          !this.state.isEditing && this.props.noCancel === "1" ?
          <span onClick={this.changeModeEdit} className="Card__edit">
            <FontAwesomeIcon icon={faEditRegular} />
          </span> : null
        }




        {/* <span onClick={this.changeModeEdit} className="Card__edit">
           {this.state.isEditing ? <FontAwesomeIcon icon={faEditSolid} /> : <FontAwesomeIcon icon={faEditRegular} />} 
          </span> */}
        {!this.state.isEditing ? (
          <div className="Card__textdiv">
            <p className="Card__textp">
              <span className="Card__textspan">Titulo: </span>
              {this.props.title}
            </p>
            <p className="Card__textp">
              <span className="Card__textspan">Description: </span>
              {this.props.description}
            </p>
          </div>
        ) : (
          <EditCard
            title={this.props.title}
            description={this.props.description}
            cardId={cardId}
            //this.props.handleEditingCard
            handleEditingCardFilter={this.handleEditingCardFilter}
          />
        )}
      </li>
    );
  }
}

Card.propTypes = {
  cardId: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onCompleteCard: PropTypes.func,
  handleFav: PropTypes.func,
};

Card.defaultProps = {
  onCompleteCard: null,
};

export default Card;
