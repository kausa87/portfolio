import React, { Component } from 'react';
import './style.scss'
export default class EditCard extends Component {
  state = {
    title: this.props.title,
    description: this.props.description,
  };

  handleOnChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  handleSaveModifications = (e) => {
    this.props.handleEditingCardFilter({
      cardId: this.props.cardId,
      title: this.state.title,
      description: this.state.description,
    });
  };

  render() {
    return (
      <div className="EditCard">
        <div className="EditCard__div">
          <label htmlFor="title" />
          <input
            className="EditCard__input"
            type="text"
            name="title"
            placeholder="Titulo"
            value={this.state.title}
            onChange={this.handleOnChange}
          />
        </div>
        <div className="EditCard__div">
          <label htmlFor="description" />
          <input
            className="EditCard__input"
            type="text"
            placeholder="Descripción"
            name="description"
            value={this.state.description}
            onChange={this.handleOnChange}
          />
        </div>
        <div className="EditCard__divbutton">
          <button
            onClick={this.handleSaveModifications}
            disabled={!this.state.title || !this.state.description}
            className="EditCard__button-add"
          >
            <span className="EditCard__text">GUARDAR</span>
          </button>
        </div>
      </div>
    );
  }
}
