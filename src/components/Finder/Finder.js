import React, { Component } from 'react';
import './style.scss';

export default class Finder extends Component {
  state = {
    title: '',
  };

  handleOnChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
    this.props.HandleFilterResult(value)
    console.log("dentro del finder", value)
  };

  render() {
    return (
      <div className="Finder">
        <label htmlFor="title" />
        <input autoComplete="off"
          className="Finder__input"
          type="text"
          name="title"
          placeholder="Buscar..."
          value={this.state.title}
          onChange={this.handleOnChange}
        />
      </div>
    );
  }
}
