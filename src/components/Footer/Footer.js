import React, { Component } from 'react'
import './style.scss';
export default class Footer extends Component {
    
    render() {
        const first = "Powered by Andrea Manca"
        const linkedin = "LinkdIn"
        const about = "About me"
        const email = "Contact"
        return (
            <div className="Footer">
                {/* <img className="Footer__img" src="https://res.cloudinary.com/uyscuty/image/upload/v1593181752/logoblackbg_bvy1mz.png" alt="logo planéalo"></img>
             */} <span className="Footer__title">{first}</span> <br/> 
        <span className="Footer__subtitle">{linkedin} | {email} | {about}</span> 
            </div>
        )
    }
}
