import React, { Component } from 'react';
import './style.scss';
import { uuid } from 'uuidv4';

class Form extends Component {
  state = {
    title: '',
    description: '',
  };

  handleOnChange = (e) => {
    const {name, value} = e.target
    this.setState({
      [name]: value,
    });
  };

  handleAddCard = () => {
    this.props.onAddCard({
      userId: this.props.isAuthenticated,
      title: this.state.title,
      description: this.state.description,
      id: uuid(),
      fav: false,
    });
    this.props.onCancel();
  };

  render() {
    return (
      <div className="Form__general">
        <div className="Form__div">
          <label htmlFor="title"/>
          <input
            className="Form__input"
            type="text"
            name="title"
            placeholder="Titulo"
            value={this.state.title}
            onChange={this.handleOnChange}
          />
        </div>
        <div className="Form__div">
          <label htmlFor="description"/>
          <input
            className="Form__input"
            type="text"
            placeholder="Descripción"
            name="description"
            value={this.state.description}
            onChange={this.handleOnChange}
          />
        </div>
        <div className="Form__divbutton">
          <button
            onClick={this.handleAddCard}
            disabled={!this.state.title || !this.state.description}
            className="Form__button-add"
          >
            Agregar una nueva tarea
          </button>
          <button onClick={this.props.onCancel} className="Form__button-cancel">
            Cancelar
          </button>
        </div>
      </div>
    );
  }
}

export default Form;
