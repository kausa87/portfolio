import React, { Component } from 'react';
import Finder from '../Finder';
// import { TransitionGroup } from 'react-transition-group';
import { PropTypes } from 'prop-types';
import Card from '../Card';
import './style.scss';
class MainList extends Component {
  state = {
    filter: '',
    filterList: this.props.list,
  };

  HandleFilterResult = async (string) => {
    // console.log('string: HandleFilterResult ', string);
    await this.setState({
      filter: string,
    });

    const myFiltered = this.props.list.filter((elem) =>
      elem.title.includes(this.state.filter)
    );
    // console.log('Finder string: ', this.state.filter);
    // console.log('Finder HandleFilterResult: ', myFiltered);
    await this.setState({
      filterList: myFiltered,
    });
  };

  render() {


    return (
      <ul className="MainList myul">
        {this.props.list.length ? null : <h3>¡Nada Pendiente! Good Job 💪🏼</h3>}

        {this.props.noCancel === "0" || this.props.noCancel === "1" ?
        <Finder HandleFilterResult={this.HandleFilterResult} /> : null }

        {this.state.filterList.map(({ id, title, fav, description }) => (
          <React.Fragment key={id}>
            <Card
              noCancel={this.props.noCancel}
              cardId={id}
              title={title}
              description={description}
              fav={fav}
              onCompleteCard={this.props.onCompleteCard}
              handleFav={this.props.handleFav}
              handleDeleting={this.props.handleDeleting}
              handleEditingCard={this.props.handleEditingCard}
            />
          </React.Fragment>
        ))}
        {/* </TransitionGroup> */}
      </ul>
    );
  }
}

MainList.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      text: PropTypes.string,
    })
  ).isRequired,
  onCompleteCard: PropTypes.func,
  handleFav: PropTypes.func,
};

MainList.defaultProps = {
  onCompleteCard: null,
};

export default MainList;
