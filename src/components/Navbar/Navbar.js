import React, { Component } from 'react'
import {Link} from 'react-router-dom'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faHome } from '@fortawesome/free-solid-svg-icons'
// import { faPlus } from '@fortawesome/free-solid-svg-icons'
import './style.scss'
// const title = "TodoList AM-version"
const logo = "https://res.cloudinary.com/uyscuty/image/upload/v1593360678/organizer_axzpxh.png"

class Navbar extends Component {
    
    render() {
        return (
            
          <nav className="Navbar aux">
            <div className="Navbar__upper">
              <img className="Navbar__img"src={logo} alt="logo planéalo"/>
              
              
              
            <div className="Navbar__down">
{ this.props.isAuthenticated ?
<React.Fragment>
            <Link to="/favorite" className="Navbar__link">
            {/* <FontAwesomeIcon icon={faHome} /> */}
            <img className="Navbar__icon" alt="favorite" src="https://res.cloudinary.com/uyscuty/image/upload/v1593283165/corazon_onhwuw.png"/>
        {/* <span className="Navbar__subt">Favoritos </span> */}
          </Link>


          <Link to="/pendingtasks" className="Navbar__link">
            {/* <FontAwesomeIcon icon={faHome} /> */}
            <img className="Navbar__icon" alt="home" src="https://res.cloudinary.com/uyscuty/image/upload/v1593283195/navegador_ularnp.png"/>
            {/* <span className="Navbar__subt">Home</span> */}
          </Link>
  

            <Link to="/completed" className="Navbar__link">
            <img className="Navbar__icon" alt="done" src="https://res.cloudinary.com/uyscuty/image/upload/v1593283179/derecho_i5ulx8.png"/>
             {/* <span className="Navbar__subt">Tareas completadas</span> */}
            </Link>
            </React.Fragment>
            : null }

            <Link to="/" className="Navbar__link">
            {/* <FontAwesomeIcon icon={faHome} /> */}
            <img className="Navbar__icon" alt="auth" src="https://res.cloudinary.com/uyscuty/image/upload/v1593345133/usuario_prcynv.png"/>
        {/* <span className="Navbar__subt">Auth </span> */}
          </Link>

          </div>
          </div>
        </nav>
  

        )
    }
}


export default Navbar
