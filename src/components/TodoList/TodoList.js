import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import Finder from '../Finder'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import MainList from '../MainList';
import Form from '../Form';

import './style.scss';

class TodoList extends Component {
  state = {
    isEditing: false,
    finder: '',
  };

  handleSetEditing = () => {
    this.setState({
      isEditing: true,
    });
  };

  handleStopEditing = () => {
    this.setState({
      isEditing: false,
    });
  };

  // HandleFilterResult = (string) =>{
  //   //this.props.todos.map( elem => elem.title.includes(string))
  //   console.log("string: ",string )
  //   this.setState({
  //     finder: string,
  //   })
  //   console.log("Finder: ", this.state.finder)
  // }

  render() {


    return (
      <div className="TodoList">
        <h2 className="TodoList__title">{this.props.title}</h2>

        <div className="TodoList__list">
        {/* <Finder HandleFilterResult={this.HandleFilterResult}/> */}
          <MainList
            noCancel={this.props.noCancel}
            list={this.props.todos}
            onCompleteCard={this.props.onCompleteCard}
            handleFav={this.props.handleFav}
            handleDeleting={this.props.handleDeleting}
            handleEditingCard={this.props.handleEditingCard}
          />

          {this.props.onCompleteCard && this.props.onAddCard ? (
            <React.Fragment>
              {this.state.isEditing ? (
                <Form
                isAuthenticated={this.props.isAuthenticated}
                  onAddCard={this.props.onAddCard}
                  onCancel={this.handleStopEditing}
                />
              ) : (
                <div className="TodoList__end">
                  <button
                    className="TodoList__add-button TodoList__icon"
                    onClick={this.handleSetEditing}
                  >
                    <FontAwesomeIcon icon={faPlus} /><span className="TodoList__text"> Añadir tarea</span>
                  </button>
                  
                </div>
              )}
            </React.Fragment>
          ) : null}
        </div>
      </div>
    );
  }
}

TodoList.propTypes = {
  title: PropTypes.string.isRequired,
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      text: PropTypes.string,
    })
  ).isRequired,
  onCompleteCard: PropTypes.func,
  onAddCard: PropTypes.func,
  handleFav: PropTypes.func,
};

TodoList.defaultProps = {
  onCompleteCard: null,
  onAddCard: null,
};

export default TodoList;
