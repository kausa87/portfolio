import low from 'lowdb';
import LocalStorage from 'lowdb/adapters/LocalStorage';

const adapter = new LocalStorage('db');
const db = low(adapter);

const todosSample = [
  { userId: "c25735ec-624d-480b-a32f-34b443a61142", id: '2werj0943isdffd', title: 'Mi tarea 2', fav: true, description: "My description 2" },
  { userId: "c25735ec-624d-480b-a32f-34b443a61143", id: '2werj0943isdffd', title: 'Mi tarea 2', fav: true, description: "My description 2" }
];

const completedSample = [
    { userId: "c25735ec-624d-480b-a32f-34b443a61142", id: '4werj0943iwlsd,d', title: 'Mi tarea 4', fav: false, description: "My description 4" }
  ];

const userSample = [
    {id: "c25735ec-624d-480b-a32f-34b443a61142", email: "amanca246@gmail.com", password: "Psw12345"}
]

// Añadimos valires por defecto a nuestra nueva DB
db.defaults({ todos: todosSample, completed: completedSample, user: userSample }).write();

export default db;
